//
// Created by Ian Norton on 2025-02-24.
//
#include <stdio.h>
#include <string.h>
#include "custom-fixtures.h"

#include <stdlib.h>
#include <time.h>

/*
 * This is a more advanced fixture example where you are able to share values from fixtures with tests.
 */

static void begin_complex_fixture(void) {
  // called on each use at the start of the fixture scope, set some values to be used during tests.
  if (complex_custom_fixture.fixture_number == 0) {
    // only seed the rng on the first ever use
    srand((unsigned int)(time(NULL) & 0xffff));
  }
  complex_custom_fixture.fixture_number = 1 + abs(rand() % 100);
  complex_custom_fixture.fixture_data = (char*)malloc(255); // free this later in the teardown
  snprintf(complex_custom_fixture.fixture_data, 255, "there are %d cats", rand() % 10);
  fprintf(stderr, "\n\t\t >> Begin Complex fixture (number=%d)\n", complex_custom_fixture.fixture_number);
}

static void end_complex_fixture(void) {
  void* buf = complex_custom_fixture.fixture_data;
  if (buf != NULL) {
    fprintf(stderr, "\n\t\t  < End Complex fixture (number=%d)\n", complex_custom_fixture.fixture_number);
    complex_custom_fixture.fixture_data = NULL;
    free(buf);
  }
}

ComplexCustomFixture complex_custom_fixture = {
    0,
    NULL,
    {
        "my complex fixture",
        CU_ScopeSuite, // teardown called at the end of the suite
        begin_complex_fixture,
        end_complex_fixture,
    }
};
