#include "ci-split-suite_yellow.h"
#include "simple-fixtures.h"
#include "custom-fixtures.h"


CU_SUITE_SETUP() {
  fprintf(stderr, "open picnic box..\n");
  CU_UseFixture(&custom_suite_fixture);
  CU_UseFixture(&complex_custom_fixture.fixture);
  return CUE_SUCCESS;
}

static void test_yellow_jacket(void) {
  fprintf(stderr, "Buzz Buzz %d\n", complex_custom_fixture.fixture_number);
  fprintf(stderr, "%s", complex_custom_fixture.fixture_data);
  fprintf(stderr, "\n");
}

static void test_yellow_cheese(void) {
  fprintf(stderr, "Yum\n");
  fprintf(stderr, "%s", complex_custom_fixture.fixture_data);
  fprintf(stderr, "\n");
}

static void test_only_windows(void) {
  CU_UseFixture(&only_windows_fixture);
  fprintf(stderr, "this only runs on windows\n");
}

static void test_not_windows(void) {
  CU_UseFixture(&not_windows_fixture);
  fprintf(stderr, "this cant run on windows\n");
}

CU_CI_AUTO_SUITE(suite_yellow,
            CUNIT_CI_TEST(test_yellow_jacket),
            CUNIT_CI_TEST(test_yellow_cheese),
            CUNIT_CI_TEST(test_only_windows),
            CUNIT_CI_TEST(test_not_windows)
            )
