//
// CUnit example fixtures Created by Ian Norton on 2025-02-24.
//

#include "CUnit/TestFixture.h"

#ifndef CUSTOM_FIXTURES_H
#define CUSTOM_FIXTURES_H

typedef struct ComplexCustomFixture {
    int fixture_number;
    char *fixture_data;
    CU_Fixture fixture;
} ComplexCustomFixture;

extern ComplexCustomFixture complex_custom_fixture;

#endif //CUSTOM_FIXTURES_H
